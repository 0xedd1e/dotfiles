;; Install company and enable globally.
(use-package company
  :ensure t
  :config
  (add-hook 'after-init-hook 'global-company-mode))

;; which-key provides info on key combinations.
(use-package which-key
  :init (which-key-mode)
  :config
  (which-key-setup-side-window-right-bottom)
  (setq which-key-sort-order 'which-key-key-order-alpha
    which-key-side-window-max-width 0.33
    which-key-idle-delay 1.0)
  :diminish which-key-mode)

;; Vertico (minimal completion on-top of standard Emacs)
(use-package vertico
	:ensure t
	:bind (:map vertico-map
							("C-j" . vertico-next)
							("C-k" . vertico-previous)
							("C-f" . vertico-exit))
	:custom
	(vertico-cycle t)
  :init
  (vertico-mode))

(use-package marginalia
	:after vertico
	:ensure t
	:custom
	;; Add some more details to the listings
	(marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
	:init
	(marginalia-mode))

;; Enable savehist mode since Vertico jacks into that and provides better results (MRU like)
;; Note: Built-in package in Emacs.
(use-package savehist
	:init
	(savehist-mode 1))

;; End of completion-config
(provide 'completion-config)
