;; Keybinding is set in keybindings-config.el

;; dired-sidebar (with dired-subtree) might be worth checking out.

;; ;; Install all-the-icons package if not installed and also install the fonts while at it!
;; (if (display-graphic-p)
;;     (unless (package-installed-p 'all-the-icons)
;;       (package-install 'all-the-icons)
;;       (all-the-icons-install-fonts)
;;       ;; Also enable icons in dired
;;       (use-package all-the-icons-dired
;; 	:hook (dired-mode . all-the-icons-dired-mode))))

;; ;; Dired config
;; (use-package dired
;;   :ensure nil
;;   :commands (dired dired-hide-details-mode)
;; ;; Bind is set in keybindings-config.el
;;   :custom ((dired-listing-switches "-Gagh --group-directories-first")))

;; ;; Default to hide details in dired buffers
;; (add-hook 'dired-mode-hook
;; 	  (lambda ()
;; 	    (dired-hide-details-mode)))



;; End of fs-explore-config
(provide 'fs-explore-config)
