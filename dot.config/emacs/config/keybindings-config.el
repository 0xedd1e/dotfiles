;; Make ESC quit prompts by running keyboard-escape-quit command
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Make CONTROL-TAB show switch-buffer menu
(global-set-key (kbd "C-<tab>") 'switch-to-buffer)

;; - Add a custom leader/prefix with some semantic-ish (to me at least) bindings.
(use-package general
	:config
	(general-evil-setup t)
	(general-create-definer eddie/leader-keys
		:keymaps '(normal insert visual emacs)
		:prefix "SPC"
		:global-prefix "C-SPC")

	(eddie/leader-keys
	 "b" '(:ignore b :which-key "B{uffer,ookmark} related")
		;; Buffer related
	 "bul" '(buffer-menu :which-key "list buffers")
	 "bus" '(switch-to-buffer :which-key "switch buffer menu")
	 "buk" '(kill-buffer :which-key "kill buffer")
		;; Bookmark related
	 "bol" '(bookmark-bmenu-list :which-key "List bookmarks")
	 ;; Directory / dired related
	 "d" '(:ignore d :which-key "Directory/dired related")
	 "dh" '((dired "~/") :which-key "Dired (Home dir)")
	 ;; Org related
	 "o" '(:ignore o :which-key "Org related")
	 "oa" '(org-agenda :which-key "agenda")
	 "oc" '(org-capture :which-key "capture")
	 )
)

;;---------------------------------------------------------------------------

;; End of keybindings-config
(provide 'keybindings-config)
