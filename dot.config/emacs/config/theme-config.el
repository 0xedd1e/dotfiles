;; Setup a decent theme
(use-package zenburn-theme
  :ensure t)

(load-theme 'zenburn t)

;; Modeline related configurations
(use-package moody
  :config
  (setq x-underline-at-descent-line t)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function))

;; End of modeline related configuration :-)

;; End of theme-config
(provide 'theme-config)
