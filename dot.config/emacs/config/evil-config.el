; Evil mode (VIM emulation)
(use-package evil
  :demand t
  :init
  (setq evil-want-keybinding nil)
	;; Make state clearly visible by changing color of the cursor.
	;; TODO: Tweak this a bit more once we've settled on a theme...
  (setq evil-emacs-state-cursor '("green" hollow))
  (setq evil-normal-state-cursor '("green" box))
  (setq evil-visual-state-cursor '("cyan" box))
  (setq evil-insert-state-cursor '("red" bar))
  (setq evil-replace-state-cursor '("red" bar))
  (setq evil-operator-state-cursor '("red" hollow))

  :config
  (evil-mode 1))
  ;; Make use of normal way to exit stuff in Emacs.
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)

;; Extend our VIM bindings to more modes etc.
(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; End of evil-config
(provide 'evil-config)
