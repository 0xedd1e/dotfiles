;;-------------------------------------
;;-- Markdown related
;;-------------------------------------
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

;;-------------------------------------
;;-- C/C++ related
;;-------------------------------------
;; Indentation
(setq c-basic-offset 2)

;;-------------------------------------
;;-- Web related
;;-------------------------------------
;; Indentation
(setq js-indent-level 2)
(setq css-indent-offset 2)

;; End of language-modes-config
(provide 'language-modes-config)
