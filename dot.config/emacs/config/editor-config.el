;; Visual bell > audible bell
(setq visible-bell t)

;; Highlight current line
(global-hl-line-mode t)

;; Display linenumbers regardless of mode
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; Display current column number
(column-number-mode)

;; Highlight matching pairs of parentheses.
(setq show-paren-delay 0)
(show-paren-mode)

;; Consider a period followed by a single space to be end of sentence.
(setq sentence-end-double-space nil)

;; Display the distance between two tab stops as 2 characters wide.
(setq-default tab-width 2)

;; Show stray whitespace.
(setq-default show-trailing-whitespace t)
(setq-default indicate-empty-lines t)
(setq-default indicate-buffer-boundaries 'left)

;; End of editor-config
(provide 'editor-config)
