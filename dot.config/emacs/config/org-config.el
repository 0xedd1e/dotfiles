;; Org related configuration

;;----------------
;; TODOS
;; - Make body text (normal text) in org a serif one?
;; - Configure org agenda
;; - Configure org file related stuff
;; - Configure capture templates
;;----------------

;; Ensure Org Mode is Loaded
(require 'org)

(defun ee/org-mode-setup()
	"Steps for setting up org mode the way I want it."

	;;(org-indent-mode)

	(visual-line-mode t)
	
	;;Enable variable-pitch fonts for body text
	(variable-pitch-mode 1)

	;; Specify specific fonts (evaluated in given order) and defaults to sans serif family.
	(let ((org-headline-font-choice
				 (cond ((x-list-fonts "Noto Sans")         "Noto Sans")
							 ((x-list-fonts "Source Sans Pro") "Source Sans Pro")
							 ((x-list-fonts "Lucida Grande")   "Lucida Grande")
							 ((x-list-fonts "Verdana")         "Verdana")
							 ((x-family-fonts "Sans Serif")    "Sans Serif")
							 (nil (progn
											(warn "Cannot find a Sans Serif Font. Install Source Sans Pro.")
											"Sans Serif")))))

		;; Font setup (scale and weights)
		(custom-set-faces
		 ;; Document title
		 `(org-document-title ((t (:inherit variable-pitch :family ,org-headline-font-choice :weight bold :height 1.7))))
		 ;; Headers 
		 `(org-level-1 ((t (:inherit variable-pitch :family ,org-headline-font-choice :weight bold :height 1.4))))
		 `(org-level-2 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold :height 1.3))))
		 `(org-level-3 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold :height 1.2))))
		 `(org-level-4 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold :height 1.1))))
		 ;; Headers > 4 (optional, can keep same size)
		 `(org-level-5 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold))))
		 `(org-level-6 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold))))
		 `(org-level-7 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold))))
		 `(org-level-8 ((t (:inherit variable-pitch :family org-headline-font-choice :weight bold))))))

	; Use monospace font for code blocks and tables
	(let ((org-monospace-font-choice
				 (cond ((x-list-fonts "JetBrains Mono")  "JetBrains Mono")
							 ((x-list-fonts "Fira Code")       "Fira Code")
							 ((x-list-fonts "Courier New")     "Courier New")
							 ((x-list-fonts "Consolas")        "Consolas")
							 ((x-list-fonts "DejaVu Sans Mono") "DejaVu Sans Mono")
							 ((x-family-fonts "Monospace")     "Monospace")
							 (nil (progn
											(warn "Cannot find a Monospace Font. Install Fira Code or JetBrains Mono.")
											"Monospace")))))
		(custom-set-faces
		 `(org-block ((t (:inherit fixed-pitch :family ,org-monospace-font-choice))))
		 `(org-code ((t (:inherit fixed-pitch :family ,org-monospace-font-choice))))
		 `(org-table ((t (:inherit fixed-pitch :family ,org-monospace-font-choice))))))


	;; Highlight code blocks with syntax highlighting
	;;(setq org-src-fontify-natively t)

	;; Folding symbol
	;;(setq org-ellipsis " [+]")
)

;; Packages improving org-mode

;; (use-package org-modern
;;   :ensure t
;;   :hook
;;   (org-mode . org-modern-mode) ;; Enable org-modern-mode in org-mode
;;  (org-mode . org-modern-table) ;; Optional: Enhance tables
;;  :custom
;;  (org-modern-star '("◉" "○" "✸" "✿")) ;; Custom bullet points
;;  (org-modern-todo nil) ;; Optional: Disable org-modern's todo styling
;;  (org-modern-table nil) ;; Optional: Default to org-modern's table styling
;;  (org-modern-label-border 0.2) ;; Optional: Adjust label border thickness
;;  (org-modern-block-fringe t) ;; Add block fringe styles
;;  :config
  ;;;(setq org-modern-hide-stars nil) ;; Show stars along with modern bullets
;;  (global-org-modern-mode)) ;; Enable globally if desired

;; Run our custom setup function for org-mode
(add-hook 'org-mode-hook 'ee/org-mode-setup)




;; 	;; Check keybindings-config.el for information regarding shortcuts that are org related (C-SPC-o {....})
;; 	(setq org-startup-indented t
;; 				;; Foldoing symbol
;; 				org-ellipsis " [+]"
;; 				;;Make org-mode hide the emphasis related markup (e.g. /.../ for italics, *...* for bold, etc.)
;; 				;org-hide-emphasis-markers nil
;; 				org-pretty-entities t
;; 				;; Make /my italicized text/ render as italic.
;; 				org-fontify-whole-heading-line t
;; 				org-fontify-done-headline t
;; 				org-fontify-quote-and-verse-blocks t)

;; 		(setq org-return-follows-link t
;;       org-agenda-tags-column 75
;;       org-deadline-warning-days 30
;;       org-use-speed-commands t)

;; 		(setq org-todo-keywords
;; 					'((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
;; 						(sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))) ;; Reason for the somewhat odd syntax is because these states require some explanation and also a timestamp will be added automagically..

;; 		;;(setq org-refile-targets '((org-agenda-files :maxlevel . 3)))

;; 		;;(setq org-capture-templates
;; 		;;			'(("t" "Todo" entry (file "~/Documents/test_gtd/inbox.org")
;; 		;;			 ;; Content of capture template
;; 		;;				"* TODO %?\n  %i\n  %a")))

;; 		;; TODO: Make some structure that can be used on work and private.
;; 		;; TODO: Check how tilde is expanded
;; 		;;(setq org-agenda-files
;; 		;;			(list
;; 		;;						"~/Documents/test_gtd/inbox.org"
;; 		;;						"~/Documents/test_gtd/projects.org"
;; 		;;						"~/Documents/test_gtd/repeaters.org"))

;; 		;; Bind SPC (" ") to render custom agenda view.
;; 		(setq org-agenda-custom-commands
;;       '((" " "Agenda"
;;          ((agenda ""
;;                   ((org-agenda-span 'day)))
;;           (todo "TODO"
;;                 ((org-agenda-overriding-header "Unscheduled tasks")
;;                  (org-agenda-files '("~/Documents/test_gtd/inbox.org"))
;;                  (org-agenda-skip-function '(org-agenda-skip-entry-if 'scheduled 'deadline))
;;                  ))
;;           (todo "TODO"
;;                 ((org-agenda-overriding-header "Unscheduled project tasks")
;;                  (org-agenda-files '("~/Documents/test_gtd/projects.org"))
;;                  (org-agenda-skip-function '(org-agenda-skip-entry-if 'scheduled 'deadline))))))))


;; 		(defmacro func-ignore (fnc)
;; 			"Return function that ignores its arguments and invokes FNC."
;; 			`(lambda (&rest _rest)
;; 				 (funcall ,fnc)))

;; 		;; Make sure org-buffers are up to date when changes are applied (explicit or implicit)
;; 		(advice-add 'org-deadline       :after (func-ignore #'org-save-all-org-buffers))
;; 		(advice-add 'org-schedule       :after (func-ignore #'org-save-all-org-buffers))
;; 		(advice-add 'org-store-log-note :after (func-ignore #'org-save-all-org-buffers))
;; 		(advice-add 'org-todo           :after (func-ignore #'org-save-all-org-buffers))

;; 		;; Ensure external changes are provided in open buffers (i.e. syncing related).
;; 		;; TODO: These shall not be here, since it is not org specifik.
;; 		;;(setq auto-save-default t
;;     ;;  auto-revert-use-notify nil
;;     ;;  auto-revert-verbose nil)

;; 		;;(global-auto-revert-mode 1)
;; )
;; End of org-config
(provide 'org-config)
