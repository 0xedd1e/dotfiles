;;---------------------------------------------------------------------------
;; New stuff goes starts here
;;---------------------------------------------------------------------------
;;(use-package projectile
;; :ensure t
;; :config
;; (define-key projectile-mode-map (kbd "C-x p") 'projectile-command-map)
;; (projectile-mode 1))

;;(use-package powerline
;;	:init
;;	(powerline-default-theme))

;; TODO:
;; - Hydra.el (possible to add custom bindings active for a given amount of time)

(use-package all-the-icons
  :if (display-graphic-p))
;; Don't forget to run: all-the-icons-install-fonts
;; On Windows this shall be done manually (i.e. install the TTF-fonts manually)

(use-package moody
  :config
  (setq x-underline-at-descent-line t)
  (moody-replace-mode-line-buffer-identification)
  (moody-replace-vc-mode)
  (moody-replace-eldoc-minibuffer-message-function))
;; We'll git is git, so let's try this well-known wrapper out.
(use-package magit)

;; Tested, tuned and almost ready for being added permanently
;;---------------------------------------------------------------------------

;; End of staging-config
(provide 'staging-config)
