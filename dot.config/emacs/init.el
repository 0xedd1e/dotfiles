;;-------------------------------------
;;-- Basic / common settings
;;-------------------------------------
(setq inhibit-startup-message t)

;; Disable {scroll, tool, menu}bar
(scroll-bar-mode -1)
(tool-bar-mode -1)
;;(menu-bar-mode -1)
;;(tooltip-mode -1)

;; Write customizations to a separate file instead of this file.
(setq custom-file (expand-file-name "customizations.el" user-emacs-directory))
(load custom-file t)

;;-------------------------------------
;;-- Package related (bootstrapping)
;;-------------------------------------
(require 'package)
(package-initialize)
(add-to-list 'package-archives
						 '("melpa" . "https://melpa.org/packages/"))
(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; Add ~/.config/emacs/config to load-path.
(add-to-list 'load-path (expand-file-name "config" user-emacs-directory))

;;-------------------------------------
;;-- Packages
;;-------------------------------------
;; Load our configured language related modes (e.g. markdown, c++ etc.)
(require 'language-modes-config)

;; Load org-mode related configurations
(require 'org-config)

;; Load work-in-progress/staging config (i.e. stuff being tested out.)
(require 'staging-config)

;;-------------------------------------
;;-- Visual/GUI and UX related
;;-------------------------------------
(require 'theme-config)
(require 'editor-config)
;(require 'evil-config)
(require 'fs-explore-config)

;; Completion related configuration(s)
(require 'completion-config)

;;-------------------------------------
;;-- Keybindings
;;-------------------------------------
(require 'keybindings-config)



;; Open bookmark listing upon start of Emacs
(bookmark-bmenu-list)
(switch-to-buffer "*Bookmark List*")
;; End of explicit config
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
